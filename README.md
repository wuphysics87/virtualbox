# virtualbox

## Start Downloading VM ISOS

1. Go to https://pop.system76.com/, click 'Download' in the top right corner, and then 'Download 22.04 LTS'
2. Next, go to https://www.microsoft.com/en-us/software-download/windows10
- If you are on Windows, click 'Download Now' under 'Create Windows 10 installation media'
    1. Follow the prompts
    2. When asked 'What do you want to do?' click on 'create installation media for another PC'
    3. Continue through the prompts until you reach 'Choose which media to use'
    4. Then click on 'ISO file'
    5. **IMPORTANT** Save in on your desktop so it is easy to find later.
- For MacOS users, you'll navigate to the same site, but you will be able to download the ISO directly
    1. Click the dropdown under 'Select Edition', and choose 'Windows 10 (multi-edition ISO)
    2. Choose English for the language
    3. Then the 64-bit Download
    4. The download will be named 'Win10_22H2_English_x64.iso' and will be downloaded to your downloads folder. Take note of this as you will need to find it quickly later.
3. The downloads of PopOS! and Windows 10 will take a while and we'll keep them running in the background as we move on to the next activities.

## The Power of Package Managers

In February a bug appeared in youtube-dl which made it non-functional.  Unfortunately, the maintainer of the primary repository ceased to maintain it.  Luckily, because it is an Open Source project, just like you've done with the techo template (misspelling deliberate), someone forked it and fixed it! This is one example of the power of Open Source Software.  The new maintainer renamed the project `yt-dlp` (short for youtube-dl-patch). Using our package managers, we will be installing:

- `yt-dlp`, the patched version of `youtube-dl`
- `glab`, a commandline tool for GitLab, 
- `exercism`, the puzzle based way to learn programming languages I mentioned
- `virtualbox`, the program we will use to use our ISOS to create virtual machines

On MacOS open a terminal and run:

```
brew install virtualbox exercism yt-dlp glab tldr
```

Windows users will need to open an **Admin Powershell**, and run:

```
choco install virtualbox yt-dlp exercism-io-cli glab tldr
```

A few notes:
- This is very good demonstration of the power of package managers. 
- Without them, you'd go to the respective sites, click download, and then run an executable 
- Package managers are *MUCH* faster and scriptable.  Our dev env was built with scripts and package managers 
- All of these programs are Open Source, meaning anyone can work on them, and you can see what they really do!

## Downloading Videos and Music with yt-dlp

Now that we have `tldr` and `yt-dlp`, run

```
tldr yt-dlp
```

To see some examples of the usage of `yt-dlp`. As you see, `yt-dlp` is a pretty sophistocated program. You can download videos, audio only, and even full playlists. The advantages of this compared to using YouTube in your web browser are endless.  Have that song you keep a tab open for? Want to put some music on your phone? Heck, you can even write a `bash` script to download a new video any time your favorite creator posts one!  

And, `yt-dlp` even works with with sites other than YouTube. If TikTok is successfully banned, using `yt-dlp` and a cloud based virtual machine, VPN, or proxy will most likely be the best way to keep watching TikTok content discretely (Provided creators can find a way to post) 

So is `yt-dlp` legal? For now it is. `yt-dlp` has been released into the Public Domain and neither Google, nor the RIAA have attempted to sue its maintainers. It just isn't worth the money to litigate in spite of the fact that they hate it.  Score one for the Good Guys!

## Exercism

Exercism is a puzzle based way to learn new programming languages. It starts slow and will teach you the syntax for flow control, functions, data structures and more.  

Before we actually use Exercism, let's start by creating a git repo on GitLab in order to version control our exercises.

1. Log in on Gitlab.com.  
2. Be sure to be in your personal page not your group (personal should be the default) 
3. Once there, click "New Project" followed by "Create Blank Project" 
4. Name your project "exercism", pick the namespace as your account, and you can choose public or private
5. Now open Codium, and clone this project into your repos directory as we have before. 

The next step will be to create an exercism account
1. Navigate to exercism.io
2. Create an account 
3. Once you log in, you will either be shown how to set exercism up locally, or you can create a token yourself
4. Either way, copy the token to your clipboard and return to your terminal
5. Just as before, we'll use `tldr` to see the available commands for `exercism` by typing `tldr exercism`
6. We'll be using the first 3 commands in order. 
7. To set your token, type:

```
exercism configure --token=your-application-token --workspace="$HOME/Documents/repos/exercism"
``` 

Where *your-application-token* should be in your clipboard, and the *workspace* is the path to your cloned exercism repo (change it to yours if you didn't clone to the path shown)  

8. To get our first exercise run:

```
exercism download --exercise=hello-world --track=python
```

9. You'll now see it in *exercism/python/hello-world*. 
10. Once you've completed the exercise, you can submit it by typing the third command from `tldr`:

```
exercism submit .
```

From within the exercise's directory.

*Note*: There are a few things that make exercism special:
- It's Free and Open Source
- It automatically checks if your solution is correct.
- It has a community with solutions you can reference.
- Unlike many programs like it, it is run locally with command line tools meaning you practice using the real tools you'll use later.

## VirtualBox and the Great Race

The final program we'll be using will be `virtualbox`. By now (or earlier) the isos will be finished downloading.  I will walk you through creating a PopOS! vm before our race.

The race will be me installing Linux using only the commandline vs the class installing Windows. Thanks to David being a smart alec, if *everyone* can get Windows installed before I can finish installing Linux, you will be rewarded with an ice cream. 

In order to level the playing field a little, in addition to installing Linux, I will also be doing a drive swap. For me the race will begin with my laptop flat on the table, face up, with a pry bar and a screw driver next to it. 

Since you'll lose precious seconds if you don't know where your Windows ISO is, I'll allow you to relocated it out of your (disgustingly messy) downloads folder to somewhere else you can find easily. Your starting line will be with VirtualBox open and your mouse cursor over the *Add* button. 

The finish line will be to open a web browser and play a video from YouTube. You are welcome to help each other, but please don't rush someone who is behind.  This is meant to be fun, not stressful.

If you win, the ice cream is yours. If you lose, you will be forced to go without ice cream while watching the TAs and I enjoy it. 



## Up Next...

- Hop onto a friend's computer from the terminal with SSH
- Power User tools and cool keyboard stuff
- Documentaries: Citizen Four (Edward Snowden), The Internet's Own Boy (Aaron Swartz), How to Radicalize a Normy (About Incels, 4chan, toxic web culture)
- Cloud computer, databases/SQL, regex/brain!@#$, webservers
- philosophy/ethics of computing/the internet. e.g. what's going on with tiktok
- requests. Something you've heard of and want to know more about. If IDK, I'll most likely know someone who does who can fill me in.
